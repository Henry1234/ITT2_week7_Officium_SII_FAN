---
Week: 07
Content: Project part 1 phase 1
Material: See links in weekly plan
Initials: MON/NISI
---

# Week 07 Continue work on minimal system

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* All groups have a setup where they can accss their raspberry from the schools network
* Everybody has joined the riot room

### Learning goals
* Inter-team communication and documentation
  * Level 1: the student can explain how to do inter-team communication
  * Level 2: the student can implement inter-team communication
  * Level 3: the student has inter-team communcation build in to their normal workflow

## Deliverables
* Mandatory weeky meetings with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)
* All groups have decided on the application they are building
* Networked raspberry pi+atmega system behind a firewall accessible using SSH

## Schedule

Monday

* 8:15 Introdution to the day, general Q/A session

    MON will go hrough the deliverables of the week.

    Next week have presentation about which project are being build. Remember to include requirements, users and other relevant information. It will be 5 slides/no text.

    A run-down and discussion on requirements for the juniper routers.

9:00 ('ish) You work

    We know that most groups have stuff from previous weeks, so we will nt claim too much of you time this week.

    Remember to come ask questions.   


Tuesday

* 8:15 Introdution to the day, general Q/A session

* 8:30 Group morning meeting

    You will meet in your groups every morning and decide on who does what. This is part of the project management, and is mandatory.
    Ordinary agenda:
    1. (5 min) Round the table: What did I do, and what did I finish?
    2. (5-10 min) Review of tasks: Are thay still relevant? do we need to add new ones?
    3. (5 min) Round the table: Claim one task each.

* 9:00 Teacher meetings

    Timeslot for you weekly 10 min. meeting with the teachers.

    Remember to book a time and have an agenda prepared.

* 9:00 You work on deliverables.

    Come ask us if you have questions.

## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/19s-itt2-project/19S_ITT2_exercises.pdf) for details.

## Comments

* To join the riot room
    1. Create a matrix.org user [here](https://riot.im/app/#/login)
    2. you need to be invited by someone already in the room, e.g. MON or NISI