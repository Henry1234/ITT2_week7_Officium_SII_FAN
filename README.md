# ITT2_week7_Officium_SII_FAN

<h2>Executive Overview</h2>
This is an exercise which is combined with the project. Each group will be responsible for the implementation of the product, as well as the final documentation 
and the Project Plan.  

<h2>What to do and when?</h2>
The groups will be responsible for doing the tasks and creating issues.

<h2>Goals</h2>
<h3>Network Topology<h3>

<table align="center"><tr><td align="center" width="9999">
<img src="/Configurations/Design%20Files/Topology.png" align="center" width="650" alt="Project icon">

</td></tr></table>

The end goal is to make minimal system where the Raspberry Pi can send data to the Junos router which then transmits the data to ATmega328 and having the vero board
to blink a LED.

Each group will have a seperate subnets in the Junos router. 

To connect to the Raspberry Pi and Junos router will be using public SSH keys from each group member. 

<h2>Communications</h2>
Communication is essential in this exercise and needs to be carefully focused on for the project to be successful.

* Scheduled stakeholder meetings on Gitlab every week
* Facebook group for regular messaging
* Project status reports
* Individual tasks for every person using the Gitlab board

<h2>Documentation</h2>

Every group will have there own projects which will include the "Project-plan.md" which contains a more detailed plan on the project, and "User-manual.md" 
which is intended for the customer as a how to use guide.

<h2>Programs</h2>
All files required to run the final product are in the "Programs" folder.


<h2>Team list</h2>

<table align="center"><tr><td align="center" width="9999">

Group | Name | Email
------|------|------
Officium|Karthiebhan Mahendran|kart0050@edu.eal.dk
Officium|Thais W. Nielsen|thai0051@edu.eal.dk
Officium|Lauris Henrijs Valdovskis|laur177n@edu.eal.dk
SII|Iliyan Stoyadinov|iliy0031@edu.eal.dk
SII|Soma Gergő Gallai|gerg0127@edu.eal.dk
SII|Ivan Draganov|ivan3026@edu.eal.dk
FAN|Adam Balazs|Missing Email
FAN|Frederik Wendelholm Klüver|Missing Email
FAN|Nick Larsen|Missing Email

</td></tr></table>